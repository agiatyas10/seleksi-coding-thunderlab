import React from 'react';
import {
	FaBars, FaSun, FaStar, FaHome, FaCheck
} from 'react-icons/fa';
import { NavLink } from "react-router-dom";
import "../style/sidebar.css";

const ICON_SIZE = 18;

function Sidebar({ visible, show, todoTask, todoImportant, todoToday }) {

	return (
		<>

			<side className={!visible ? 'sidebar side-top' : 'side-top'}>
				<div>
					<div>
						{
							visible ?
								<></> :
								<button
									type="button"
									className="side-btn"
									onClick={() => show(!visible)}
								>
									{<FaBars size={30} />}
								</button>
						}
						<div className="icon-bar">
							<FaBars size={ICON_SIZE} onClick={() => show(!visible)} />
						</div>
						<NavLink to="/" className="side-link">
							<FaHome size={ICON_SIZE} />
							<span>Tasks</span><span className={'notif'}>{todoTask === 0 ? '' : todoTask}</span>
						</NavLink>
						<NavLink to="/important" className="side-link">
							<FaStar size={ICON_SIZE} />
							<span>Important </span><span className={'notif'}>{todoImportant === 0 ? '' : todoImportant}</span>
						</NavLink>
						<NavLink to="/today" className="side-link">
							<FaSun size={ICON_SIZE} />
							<span>Today</span><span className={'notif'}>{todoToday === 0 ? '' : todoToday}</span>
						</NavLink>
						<div className="divider"></div>
						{/* <NavLink to="/completed" className="side-link">
							<FaCheck size={ICON_SIZE} color={"#2564cf"} />
							<span>Completed</span>
						</NavLink> */}


					</div>
				</div>

				{/* <div className="links">
					<NavLink to="/settings" className="side-link">
						<FaCog size={ICON_SIZE} />
						<span>Settings</span>
					</NavLink>
					<NavLink to="/Sign-out" className="side-link">
						<FaSignOutAlt size={ICON_SIZE} />
						<span>Logout</span>
					</NavLink>
				</div> */}
			</side>
		</>
	);
}

export default Sidebar;
