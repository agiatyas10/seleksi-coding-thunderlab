import React, { useState } from 'react';
import { v4 as uuidv4 } from 'uuid';
import InputTodo from './InputTodo';
import TodosList from './TodosList';
import '../style/todoitem.css';



const TodoToday = (props) => {

  const [todosToday, setTodosToday] = useState(JSON.parse(localStorage.getItem("listtodoToday")) ? JSON.parse(localStorage.getItem("listtodoToday")) : []);

  const { todo } = props;
  todo(todosToday);
  localStorage.setItem('todosToday', JSON.stringify(todosToday))

  const handleChange = (id) => {
    setTodosToday((prevState) => prevState.map((todo) => {
      if (todo.id === id) {
        return {
          ...todo,
          completed: !todo.completed,
        };
      }
      return todo;
    }))
  };

  const delTodo = (id) => {
    setTodosToday([...todosToday.filter((todo) => todo.id !== id)]);
  };


  const setUpdate = (updatedTitle, id) => {
    setTodosToday(
      todosToday.map((todo) => {
        if (todo.id === id) {
          // eslint-disable-next-line no-param-reassign
          todo.title = updatedTitle;
        }
        return todo;
      }),
    );

  };

  const addTodoItem = (title) => {
    const newTodo = {
      id: uuidv4(),
      title,
      completed: false,
    };
    setTodosToday([...todosToday, newTodo]);

  };

  return (
    <div className={'container'}>
      {/* input todo */}
      <InputTodo addTodoProps={addTodoItem} />

      {/* Todolist */}
      <TodosList
        todos={todosToday}
        handleChangeProps={handleChange}
        deleteTodoProps={delTodo}
        setUpdate={setUpdate}
      />
    </div>
  );
};

export default TodoToday;
