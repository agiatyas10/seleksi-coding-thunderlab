import React from 'react';
import { FaTh } from 'react-icons/fa';


import "../style/navbar.css";

const ICON_SIZE = 18;

function Navbar({ show }) {

    return (
        <>
            <div className='header-nav nav-link'>
                <FaTh size={ICON_SIZE} />
                <span style={{ marginLeft: "20px" }}>TO - DO </span>

            </div>
            <div style={{ float: "right", color: "#fff" }}>
                <FaTh size={ICON_SIZE} />
            </div>
        </>
    );
}

export default Navbar;
