import React, { useState } from 'react';
import { v4 as uuidv4 } from 'uuid';
import InputTodo from './InputTodo';
import TodosList from './TodosList';
import '../style/todoitem.css';

const TodoTask = (props) => {
    const [todosTask, setTodosTask] = useState(JSON.parse(localStorage.getItem("listtodoTask")) ? JSON.parse(localStorage.getItem("listtodoTask")) : []);
    const [todoStarred, setTodoStarred] = useState([]);
    const { todo } = props;
    todo(todosTask);
    localStorage.setItem('todosTask', JSON.stringify(todosTask))

    const handleChange = (id) => {
        setTodosTask((prevState) => prevState.map((todo) => {
            if (todo.id === id) {
                return {
                    ...todo,
                    completed: !todo.completed,
                };
            }
            return todo;
        }))
    };

    const delTodo = (id) => {
        setTodosTask([...todosTask.filter((todo) => todo.id !== id)]);
    };

    const starTodo = (id) => {
        setTodoStarred([...todosTask.filter((todo) => todo.id === id)])
        setTodosTask([...todosTask.filter((todo) => todo.id !== id)]);
    }


    const setUpdate = (updatedTitle, id) => {
        setTodosTask(
            todosTask.map((todo) => {
                if (todo.id === id) {
                    // eslint-disable-next-line no-param-reassign
                    todo.title = updatedTitle;
                }
                return todo;
            }),
        );

    };

    const addTodoItem = (title) => {
        const newTodo = {
            id: uuidv4(),
            title,
            completed: false,
        };
        setTodosTask([...todosTask, newTodo]);

    };

    return (
        <div className={'container'}>
            {/* input todo */}
            <InputTodo addTodoProps={addTodoItem} />

            {/* Todolist */}
            <TodosList
                starred={todoStarred}
                todos={todosTask}
                handleChangeProps={handleChange}
                deleteTodoProps={delTodo}
                handleStarProps={starTodo}
                setUpdate={setUpdate}
            />

        </div>
    );
};

export default TodoTask;
