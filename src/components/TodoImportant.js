import React, { useState } from 'react';
import { v4 as uuidv4 } from 'uuid';
import InputTodo from './InputTodo';
import TodosList from './TodosList';
import '../style/todoitem.css';


const TodoImportant = (props) => {
    const [todosImportant, setTodosImportant] = useState(JSON.parse(localStorage.getItem("listtodoImportant")) ? JSON.parse(localStorage.getItem("listtodoImportant")) : []);

    const { todo, important } = props;
    todo(todosImportant);
    localStorage.setItem('todosImportant', JSON.stringify(todosImportant))

    const handleChange = (id) => {
        setTodosImportant((prevState) => prevState.map((todo) => {
            if (todo.id === id) {
                return {
                    ...todo,
                    completed: !todo.completed,
                };
            }
            return todo;
        }))
    };

    const delTodo = (id) => {
        setTodosImportant([...todosImportant.filter((todo) => todo.id !== id)]);
    };

    const setUpdate = (updatedTitle, id) => {
        setTodosImportant(
            todosImportant.map((todo) => {
                if (todo.id === id) {
                    // eslint-disable-next-line no-param-reassign
                    todo.title = updatedTitle;
                }
                return todo;
            }),
        );

    };

    const addTodoItem = (title) => {
        const newTodo = {
            id: uuidv4(),
            title,
            completed: false,
        };
        setTodosImportant([...todosImportant, newTodo]);

    };


    return (
        <div className={'container'}>
            {/* input todo */}
            <InputTodo addTodoProps={addTodoItem} />

            {/* Todolist */}
            <TodosList
                important={important}
                todos={todosImportant}
                handleChangeProps={handleChange}
                deleteTodoProps={delTodo}
                setUpdate={setUpdate}
            />
        </div>
    );
};

export default TodoImportant;
