import React, { useState } from 'react';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import {
	FaSun, FaStar, FaHome, FaCheck
} from 'react-icons/fa';
import moment from 'moment';

import "./style/index.css";
import Navbar from "./components/Navbar";
import Sidebar from "./components/Sidebar";
import TodoTask from "./components/TodoTask";
import TodoImportant from "./components/TodoImportant";
import TodoToday from "./components/TodoToday";



function App() {
	const [navVisible, showSidebar] = useState(true);
	const ICON_SIZE = 24;
	const [todoTask, setTodoTask] = useState(0);
	const [todoImportant, setTodoImportant] = useState(0);
	const [todoToday, setTodoToday] = useState(0);

	const setTaskStorage = (e) => {
		localStorage.setItem('listtodoTask', JSON.stringify(e));
	};

	const setImportanttorage = (e) => {
		localStorage.setItem('listtodoImportant', JSON.stringify(e));
	};
	const setTodaytorage = (e) => {
		localStorage.setItem('listtodoToday', JSON.stringify(e));
	};


	return (
		<BrowserRouter >
			<div className="App">
				<Sidebar visible={navVisible} show={showSidebar} todoTask={todoTask} todoImportant={todoImportant} todoToday={todoToday} />
				<Navbar />
				<Routes>
					<Route path='/' element={
						<div className={!navVisible ? "content" : "content content-with-sidebar"}>
							<div className="title">
								<FaHome size={ICON_SIZE} />
								<span>Tasks</span>
							</div>
							<TodoTask todo={(e) => {
								setTaskStorage(e)
								setTodoTask(e.length)
							}}
								important={false}
							/>
						</div>
					} />

					<Route path='/important' element={
						<div className={!navVisible ? "content" : "content content-with-sidebar"}>
							<div className="title">
								<FaStar size={ICON_SIZE} />
								<span>Important</span>
							</div>

							<TodoImportant todo={(e) => {
								setImportanttorage(e)
								setTodoImportant(e.length)

							}}
								important={true}
							/>
						</div>
					} />

					<Route path='/today' element={
						<div className={!navVisible ? "content" : "content content-with-sidebar"}>
							<div className="title">
								<FaSun size={ICON_SIZE} />
								<span>Today</span><br></br><br></br>
							</div>

							<TodoToday todo={(e) => {
								setTodaytorage(e)
								setTodoToday(e.length)
							}}
								important={false}
							/>
							<div>{moment().format("DD/MM/YYYY")}</div>

						</div>

					} />
					<Route path='/completed' element={
						<div className={!navVisible ? "content" : "content content-with-sidebar"}>
							<div className="title">
								<FaCheck size={ICON_SIZE} />
								<span>Completed</span><br></br><br></br>
							</div>



						</div>

					} />


				</Routes>
			</div>
		</BrowserRouter>
	);
}

export default App;
